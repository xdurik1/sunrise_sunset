from flask_wtf import FlaskForm
from datetime import date
from wtforms.fields.html5 import DateField
from wtforms.fields import SelectField


class TestForm(FlaskForm):
    searchDate = DateField('Date', default=date.today)
    country = SelectField(u'Country', choices=[], coerce=str)