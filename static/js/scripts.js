$(document).ready(function(){

    //onsubmit event - show sunrise&sunset on specific place&date
    $('form').submit(function(event) {
        event.preventDefault();
        $.ajax({
            url: '/dateExample',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                $(".sunrise").html("<i class=\"far fa-sun\"></i> Sunrise is at " + response.sunrise );
                $(".sunset").html("<i class=\"far fa-moon\"></i> Sunset is at " + response.sunset );
                // $(".sunrise").html("<i class=\"far fa-sun\"></i> Sunrise is at " + response.results.sunrise );
                // $(".sunset").html("<i class=\"far fa-moon\"></i> Sunset is at " + response.results.sunset );
                $("hr").show().css('width', '75%');

            },
            error: function(error) {
                console.log(error);
            }
        });
    });

});