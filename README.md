
## Install and run project
git clone https://xdurik1@bitbucket.org/xdurik1/sunrise_sunset.git

cd sunrise_sunset

pip install -r requirements.txt

python app.py # run on 127.0.0.1:5000

## How run the tests?
pytest -ra

