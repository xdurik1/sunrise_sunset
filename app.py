from flask import Flask, render_template, request
import Forms, functions

app = Flask(__name__)
app.config['SECRET_KEY']='\xea\x18\xd4\xaa\xec"\x0e0\x92\xb9|P\xb0q\xf7\xa5\x1b\x83\xf5\x00"\xe0\x13\xaf'

@app.route('/dateExample',  methods=['GET', 'POST'])
def hello_world():
    try:
        searchDate = request.form['searchDate'];
        country = request.form['country'];

        latitude = functions.get_latitude(country)
        longitude = functions.get_longitude(country)

        result = functions.get_sunrise_sunset(searchDate, latitude, longitude)

        return result

    except NameError as e:
        return (str(e))


@app.route('/', methods=['GET', 'POST'])
def index():
    try:
        form = Forms.TestForm()
        form.country.choices = functions.get_options()

        return render_template('homepage.html', form=form)

    except NameError as e:
        return (str(e))

if __name__ == '__main__':
    app.run(debug=True)
