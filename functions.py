import csv
import requests as reqs
from datetime import timedelta, datetime

def get_options():
    with open('./static/countries.csv', 'r') as read_obj:
        csv_dict_reader = csv.DictReader(read_obj)
        choices = [(row['name']) for row in csv_dict_reader]
    return choices

def get_longitude(country):
    with open('./static/countries.csv', 'r') as read_obj:
        csv_dict_reader = csv.DictReader(read_obj)
        for row in csv_dict_reader:
            if row['name'] == country:
                longitude = row['longitude']
    return longitude

def get_latitude(country):
    with open('./static/countries.csv', 'r') as read_obj:
        csv_dict_reader = csv.DictReader(read_obj)
        for row in csv_dict_reader:
            if row['name'] == country:
                latitude = row['latitude']
    return latitude

def get_sunrise_sunset(searchDate, latitude, longitude):
    result = reqs.get(
        'https://api.sunrise-sunset.org/json?lat=' + latitude + '&lng=' + longitude + '&date=' + searchDate)

    result = result.json()

    sunrise = result['results']['sunrise']
    sunset = result['results']['sunset']


    sunset = add_hour(sunset,1)
    sunrise = add_hour(sunrise, 1)

    result = {
        'sunrise': sunrise,
        'sunset': sunset
    }

    return result

def add_hour(time, hour):
    time = datetime.strptime(time, '%I:%M:%S %p')
    time += timedelta(hours=hour)

    return time.strftime('%H:%M')